(function($){
    Drupal.behaviors.content_menu_i18n ={
        attach:function(context, settings){
            // Show menu item dialog
            $('.menu-overview-title-edit-trigger', context).click(function(e) {
                var mlid = $(this).siblings('.form-type-textfield').children('input').attr('name').replace('[title]', '').replace(':', '');
                var linkTitle = $(this).siblings('.menu-overview-title-link').text();
                var currentDialog = "#i18n_strings_container_" + mlid;

                if ($(currentDialog).length > 0) {
                  // start the dialog
                  $(currentDialog).dialog({
                    title: Drupal.t('Edit title for ') + linkTitle,
                    modal: true,
                    width: 600,
                    minHeight: 400,
                    buttons: [{text: Drupal.t("Done"), click: function() { $(this).dialog("close"); }}],
                    open: function(event, ui) {
                      $(this).siblings('.ui-dialog-buttonpane').find('.ui-dialog-buttonset').prepend('<span style="margin-right: 15px;">' + Drupal.t('Please beware that changes are not saved until the complete page is saved.') + '</span>');
                    },
                    close: function(event, ui) {
                      // remove the dialog code again
                      $(this).dialog('destroy');
                      // restore the form elements to its old position
                      $(currentDialog).appendTo('#edit-i18n-strings-container .fieldset-wrapper').hide();

                      currentDialog = '';
                     }
                  });
                }
                else {
                  // fallback to default behaviour
                  // Show menu item title input field when clicking on the trigger.
                  $(this).siblings('.form-type-textfield').show().children('input').focus();
                  $(this).siblings('.menu-overview-title-link').hide();
                  $(this).hide();
                }

                e.stopPropagation();
                e.stopImmediatePropagation();
            });
        },
    };})
(jQuery);
